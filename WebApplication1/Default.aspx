﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebApplication1.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

        <div class="content">
        <div class="box box-default">
            <div class="box-body">
                <div class="row">
                    <div class="col-xs-6 col-xs-offset-1">
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <h5 style="color: #333;">Property Information</h5>
                            </div>
                            <div class="panel-body" style="min-height: 500px;">
                                <table id="tblData" class="table">
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <h5 style="color: #333;">Tagging</h5>
                            </div>
                            <div class="panel-body" style="min-height: 500px;">
                                <div class="row" style="margin-top: 10%;">
                                    <div class="col-xs-12">
                                        <label>Is this the correct Municipality Name? &nbsp;&nbsp;<a href="#" style="display: none;" id="yes">Yes</a> &nbsp; <a href="#" style="display: none;" id="no">No</a></label>
                                    </div>
                                    <div class="col-xs-12">
                                        <%-- <select id="municipality"  class="form-control input-sm" style="width: 50%;">
                                <option value="" selected="selected">--Select One--</option>
                                
                            </select>--%>
                                        <select id="municipality" class="form-control input-sm select2" style="width: 50%;" disabled="disabled">
                                            <option value="" selected="selected">--Select One--</option>

                                        </select>
                                        <%--<label id="error" style="display:none; color:red;">Municipality Not Found</label>--%>
                                    </div>
                                </div>
                                <div class="row" style="margin-top: 10%;">
                                    <div class="col-xs-12">
                                        <label>Registration Status:</label>
                                    </div>
                                    <div class="col-xs-12">
                                        <select id="ReqStat" class="form-control input-sm" style="width: 50%;" disabled="disabled">
                                            <option value="" selected="selected">--Select One--</option>
                                            <option value="Select">Select</option>
                                            <option value="To be registered">To be registered</option>
                                            <option value="On Hold">On Hold</option>
                                            <option value="Suspended">Suspended</option>
                                            <option value="Not Yet Eligible">Not Yet Eligible</option>
                                            <option value="Cancelled">Cancelled</option>
                                            <option value="Closed">Closed</option>
                                        </select>
                                    </div>
                                </div>
                                 <div class="row" style="margin-top: 10%;">
                                    <div class="col-xs-12">
                                        <label>Registration Process:</label>
                                    </div>
                                    <div class="col-xs-12">
                                        <select id="ReqProg" class="form-control input-sm" style="width: 50%;" disabled="disabled">
                                            <option value="" selected="selected">--Select One--</option>
                                            <option value="Online">Online</option>
                                            <option value="PDF">PDF</option>
                                            <option value="Prochamps">Prochamps</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row" style="margin-top: 10%;">
                                    <div class="col-xs-12">
                                        <label>Reason:</label>
                                    </div>
                                    <div class="col-xs-12">
                                        <div style="display: none" id="001">
                                            <select id="reasonDD1" class="form-control input-sm" style="width: 50%;">
                                                <option value="" selected="selected">--Select One--</option>
                                                <option value="Waiting for OCN Information">Waiting for OCN Information</option>
                                                <option value="Waiting for City Information">Waiting for City Information</option>
                                                <option value="Waiting for Vendor ID">Waiting for Vendor ID</option>
                                            </select>
                                        </div>

                                        <div style="display: none" id="002">
                                            <select id="reasonDD2" class="form-control input-sm" style="width: 50%;">
                                                <option value="" selected="selected">--Select One--</option>
                                                <option value="No foreclosure filing">No foreclosure filing</option>
                                            </select>
                                        </div>

                                        <div style="display: none" id="003">
                                            <select id="reasonDD3" class="form-control input-sm" style="width: 50%;">
                                                <option value="" selected="selected">--Select One--</option>
                                                <option value="Occupancy requirement not met">Occupancy requirement not met</option>
                                                <option value="Registration Condition Not Met">Registration Condition Not Met</option>
                                            </select>
                                        </div>

                                        <div style="display: none" id="004">
                                            <select id="reasonDD4" class="form-control input-sm" style="width: 50%;">
                                                <option value="" selected="selected">--Select One--</option>
                                                <option value="Municipality has no ordinance">Municipality has no ordinance</option>
                                                <option value="Property type requirement not met">Property type requirement not met</option>
                                                <option value="REO Registrations Only">REO Registrations Only</option>
                                                <option value="PFC Registrations Only">PFC Registrations Only</option>
                                            </select>
                                        </div>
                                        <div style="display: none" id="006">
                                            <select id="reasonDD5" class="form-control input-sm" style="width: 50%;">
                                                <option value="" selected="selected">--Select One--</option>
                                                <option value="Registered by 3rd party">Registered by 3rd party</option>
                                            </select>
                                        </div>
                                        <div style="display: none" id="007">
                                            <select id="reasonDD7" class="form-control input-sm" style="width: 50%;">
                                                <option value="" selected="selected">--Select One--</option>
                                                <option value="For delisting">For delisting</option>
                                                <option value="For one time registration only">For one time registration only</option>
                                            </select>
                                        </div>
                                        <div id="005">
                                            <select id="reasonDD4as" class="form-control input-sm" style="width: 50%;" disabled="disabled">
                                                <option value="" selected="selected">--Select One--</option>
                                            </select>
                                        </div>

                                    </div>
                                </div>

                                <div class="row" style="margin-top: 10%;">
                                    <div class="col-xs-12 text-center">
                                        <button <%--href="Default.aspx"--%> disabled="disabled" id="saveTag" class="btn btn-default" style="width: 200px;">Save</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script src="js/Default.js"></script>
</asp:Content>
