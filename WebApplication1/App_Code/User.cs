﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RPM.App_Code
{
    public class User
    {
    }

    public class UserDetails
    {
        public string userid { get; set; }
        public string empno { get; set; }
        public string fname { get; set; }
        public string lname { get; set; }
        public string ntlogin { get; set; }
        public string department { get; set; }
        public string role { get; set; }
        public string supervisor { get; set; }
        public string email { get; set; }
        public string access { get; set; }
        public string isdeleted { get; set; }
    }
}