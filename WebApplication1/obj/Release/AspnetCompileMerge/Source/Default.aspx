﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebApplication1.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="content" style="">
        <div class="row">

            <div class="col-xs-12">
                <div class="box box-default">

                    <div class="box-body">

                        <div class="col-xs-12">
                            <h1>VPR InFlowScope</h1>
                        </div>
                        <div class="col-xs-6">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3>Property Information</h3>
                                </div>
                                <div class="panel-body" style="min-height: 600px;">
                                    <table id="tblData" class="table">
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-4">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3>Tagging</h3>
                                </div>
                                <div class="panel-body" style="min-height: 600px;">
                                    <div class="row" style="margin-top: 10%;">
                                        <div class="col-xs-12">
                                            <label>Is this the correct Municipality Name? &nbsp;&nbsp;<a href="#" style="display:none;" id="yes">Yes</a> &nbsp; <a href="#" style="display:none;" id="no">No</a></label>
                                        </div>
                                        <div class="col-xs-12">
                                            <%-- <select id="municipality"  class="form-control input-sm" style="width: 50%;">
                                <option value="" selected="selected">--Select One--</option>
                                
                            </select>--%>
                                            <select id="municipality" class="form-control input-sm select2" style="width: 50%;" disabled="disabled">
                                                <option value="" selected="selected">--Select One--</option>

                                            </select>
                                            <%--<label id="error" style="display:none; color:red;">Municipality Not Found</label>--%>

                                        </div>
                                    </div>
                                    <div class="row" style="margin-top: 10%;">
                                        <div class="col-xs-12">
                                            <label>Registration Status:</label>
                                        </div>
                                        <div class="col-xs-12">
                                            <select id="ReqStat" class="form-control input-sm" style="width: 50%;"  disabled="disabled">
                                                <option value="" selected="selected">--Select One--</option>
                                                <option value="To be registered">To be registered</option>
                                                <option value="Pending">Pending</option>
                                                <option value="Suspended">Suspended</option>
                                                <option value="Not Yet Eligible">Not Yet Eligible</option>
                                                <option value="Cancelled">Cancelled</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-top: 10%;">
                                        <div class="col-xs-12">
                                            <label>Registration Process:</label>
                                        </div>
                                        <div class="col-xs-12">
                                            <select id="ReqProg" class="form-control input-sm" style="width: 50%;" disabled="disabled">
                                                <option value="" selected="selected">--Select One--</option>
                                                <option value="Online">Online</option>
                                                <option value="PDF">PDF</option>
                                                <option value="Prochamps">Prochamps</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-top: 10%;">
                                        <div class="col-xs-12">
                                            <label>Reason:</label>
                                        </div>
                                        <div class="col-xs-12">
                                            <div style="display:none" id="001">
                                                 <select id="reasonDD1" class="form-control input-sm" style="width: 50%;">
                                                <option value="" selected="selected">--Select One--</option>
                                                <option value="Waiting for OCN Information" >Waiting for OCN Information</option>
                                                <option value="Waiting for City Information">Waiting for City Information</option> 
                                            </select>
                                            </div>
                                           
                                            <div style="display:none" id="002">
                                                  <select id="reasonDD2"  class="form-control input-sm" style="width: 50%;">
                                                <option value="" selected="selected">--Select One--</option>
                                                <option value="No foreclosure filing">No foreclosure filing</option>
                                            </select>
                                            </div>
                                          
                                            <div style="display:none" id="003">
                                                <select id="reasonDD3" class="form-control input-sm" style="width: 50%;">
                                                <option value="" selected="selected">--Select One--</option>
                                                <option value="Occupancy requirement not met">Occupancy requirement not met</option>
                                            </select>
                                            </div>
                                            
                                            <div style="display:none" id="004">
                                                <select id="reasonDD4" class="form-control input-sm" style="width: 50%;">
                                                <option value="" selected="selected">--Select One--</option>
                                                <option value="Municipality has no ordinance">Municipality has no ordinance</option>
                                            </select>
                                            </div>
                                            <div  id="005">
                                                <select id="reasonDD4as" class="form-control input-sm" style="width: 50%;" disabled="disabled">
                                                <option value="" selected="selected">--Select One--</option>
                                            </select>
                                            </div>
                                            
                                        </div>
                                    </div>
                                    <div class="row" style="margin-top: 10%;">
                                        <div class="col-xs-12 text-center">
                                            <button <%--href="Default.aspx"--%> disabled="disabled" id="saveTag" class="btn btn-default" style="width: 200px;" >Save</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script src="js/Default.js"></script>
</asp:Content>
