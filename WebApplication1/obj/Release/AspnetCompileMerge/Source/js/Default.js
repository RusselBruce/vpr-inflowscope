﻿$(document).ready(function () {

    var user = $('#usr').text();

    getSelect(user);
    
    $('.select2').select2();
});

window.onbeforeunload = function () {

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "Default.aspx/CloseUpdate",
        success: function (response) {
            console.log(response.responseText);
        },
        error: function (response) {
            console.log(response.responseText);
        }
    });

    return "Are you sure?";
};

$("#saveTag").click(function () {

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "Default.aspx/Save",
        data: '{"munitext": "' + $("#municipality option:selected").text() +
            '", "municode":"' + $("#municipality").val() +
            '","regstat":"' + $("#ReqStat").val() +
            '","regprog":"' + $("#ReqProg").val() +
            '","reason":"' + sessionStorage.getItem("Reason") +
            '","id":"' + sessionStorage.getItem("propID") +
            '","user":"' + $('#usr').text() +
            '"}',
        success: function (data) {
            var obj = data.d;
            console.log(obj);
            alertify.success("Record saved successfully.");

            location.href = "Default.aspx";
        },
        error: function (response) {
            alertify.error("Error saving in database.");
        }
    })
    });

//$("#ReqProg").on('change', function (evt, params) {
//    if ($("#ReqProg").val('')) {
//        document.getElementById("saveTag").disabled = true;
//    }
//})

$("#ReqProg").on('change', function (evt, params) {

    if ($("#ReqProg").val() != '') {
        document.getElementById("saveTag").disabled = false;
    } else {
        document.getElementById("saveTag").disabled = true;
    }

})



$('#ReqStat').on('change', function (evt, params) {

    if ($("#ReqStat").val() == '') {
        document.getElementById("saveTag").disabled = true;
    }

    if ($("#ReqStat").val() == "To be registered")
    {
        document.getElementById("ReqProg").disabled = false;
        $("#001").hide();
        $("#002").hide();
        $("#003").hide();
        $("#004").hide();
        $("#005").show();
        
    }
    else
    {
        document.getElementById("ReqProg").disabled = true;
    }
    if ($("#ReqStat").val() == "Pending")
    {
        if ($("#001:visible").length == 0) {
            $("#001").show();
            $("#002").hide();
            $("#003").hide();
            $("#004").hide();
            $("#005").hide();
           
        }
        
        $("#ReqProg").val('');
    }
    else if ($("#ReqStat").val() == "Suspended")
    {
        if ($("#002:visible").length == 0) {
            $("#001").hide();
            $("#002").show();
            $("#003").hide();
            $("#004").hide();
            $("#005").hide();
    
        }
        $("#ReqProg").val('');
    }
    else if ($("#ReqStat").val() == "Not Yet Eligible")
    {
        if ($("#003:visible").length == 0) {
            $("#001").hide();
            $("#002").hide();
            $("#003").show();
            $("#004").hide();
            $("#005").hide();
           
        }
        $("#ReqProg").val('');
    }
    else if ($("#ReqStat").val() == "Cancelled")
    {
        if ($("#004:visible").length == 0) {
            $("#001").hide();
            $("#002").hide();
            $("#003").hide();
            $("#004").show();
            $("#005").hide();
            
        }
        $("#ReqProg").val('');
    }
    else
    {
            sessionStorage.setItem("Reason", "");
    }
});


$('#reasonDD4').on('change', function (evt, params) {

    if ($("#reasonDD4").val() != '') {
        document.getElementById("saveTag").disabled = false;
    } else {
        document.getElementById("saveTag").disabled = true;
    }

    sessionStorage.setItem("Reason", $("#reasonDD4").val());

    
});
$('#reasonDD3').on('change', function (evt, params) {

    if ($("#reasonDD3").val() != '') {
        document.getElementById("saveTag").disabled = false;
    } else {
        document.getElementById("saveTag").disabled = true;
    }

    sessionStorage.setItem("Reason", $("#reasonDD3").val());

});

$('#reasonDD2').on('change', function (evt, params) {

    if ($("#reasonDD2").val() != '') {
        document.getElementById("saveTag").disabled = false;
    } else {
        document.getElementById("saveTag").disabled = true;
    }

    sessionStorage.setItem("Reason", $("#reasonDD2").val());

});

$('#reasonDD1').on('change', function (evt, params) {
    if ($("#reasonDD1").val() != '') {
        document.getElementById("saveTag").disabled = false;
    } else {
        document.getElementById("saveTag").disabled = true;
    }

    sessionStorage.setItem("Reason", $("#reasonDD1").val());
});

function getSelect(user) {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        url: 'Default.aspx/GetMunicipality',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            $('#municipality').empty;
            var d = $.parseJSON(data.d);

            if (d.Success) {
                var records = d.data.record;

                $.each(records, function (idx, val) {
                    $('#municipality').append(
                        '<option value="' + val.municipality_code + '">' + val.Municipality + '</option>'
                    );
                });

                getData(user);
            }
            $('#modalLoading').modal('hide');
        },
        error: function (response) {
            console.log(response.responseText);
        }
    });

}

function getData(user) {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });

    $.ajax({
        type: 'POST',
        url: 'Default.aspx/GetData',
        data: "{'user': '"+user+"'}",
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);

            if (d.Success) {
                var record = d.data.record;

                sessionStorage.setItem("propID", record[0].propID);

                $('#tblData tbody').empty();
                $('#tblData tbody').append(
                    '<tr>' +
                        '<th>Property ID</td>' +
                        '<td>' + record[0].propID + '</td>' +
                    '</tr>' +
                    '<tr>' +
                        '<th>Property Address</td>' +
                        '<td>' + record[0].propAddress + '</td>' +
                    '</tr>' +
                    
                    '<tr>' +
                        '<th>Municipality Name</td>' +
                        '<td>' + record[0].municipality + '</td>' +
                    '</tr>' +
                    '<tr>' +
                        '<th>Occupancy</td>' +
                        '<td>' + record[0].occStat + '</td>' +
                    '</tr>' +
                    '<tr>' +
                        '<th>FC/FCH Codes</td>' +
                        '<td>' + record[0].fcCodes + '</td>' +
                    '</tr>' +
                    '<tr>' +
                        '<th>Service Type</td>' +
                        '<td>' + record[0].servType + '</td>' +
                    '</tr>' +
                    '<tr>' +
                        '<th>Product</td>' +
                        '<td>' + record[0].prod + '</td>' +
                    '</tr>' +
                    '<tr>' +
                        '<th>POD/Portfolio</td>' +
                        '<td>' + record[0].pod + '</td>' +
                    '</tr>' 
                    //'<tr>' +
                    //    '<th>PFC Registration/REO Registration Condition</td>' +
                    //    '<td>' + record[0].regCondition + '</td>' +
                    //'</tr>'
                );

                var muni = d.data.record2;
                
                if (muni.length == 0) {
                    alert("Municipality Not Found!");
                    document.getElementById("municipality").disabled = true;
                    document.getElementById("ReqStat").disabled = false;
                    $('#municipality').val() = "";

                } else {

                    $('#municipality').select2().val(muni[0].municipality_code).trigger('change');

                    $("#yes").show();
                    $("#no").show();
                }

                if (record[0].prod == "PFC") {
                    $('#tblData tbody').append(
                  '<tr>' +
                      '<th>PFC Registration Condition</td>' +
                      '<td>' + record[0].pfcRegCond + '</td>' +
                  '</tr>' 
                  )

                }
                else if (record[0].prod == "REO") {
                    $('#tblData tbody').append(
                  '<tr>' +
                      '<th>REO Registration Condition</td>' +
                      '<td>' + record[0].reoRegCond + '</td>' +
                  '</tr>'
                  )
                }

                $.each($('#tblData tbody tr'), function (idx, val) {
                    if ($(this).find('td:contains("null")').length > 0) {
                        $(this).find('td').html('NULL');
                    }
                });
            }
            $('#modalLoading').modal('hide');
        },
        error: function (response) {
            console.log(response.responseText);
        }
    })
    
}

$("#no").click(function () {
    document.getElementById("municipality").disabled = false;
    document.getElementById("ReqStat").disabled = false;
    $("#yes").hide();
    $("#no").hide();
})
$("#yes").click(function () {
    document.getElementById("ReqStat").disabled = false;
    document.getElementById("municipality").disabled = true;
    $("#yes").hide();
    $("#no").hide();
})

function asd() {
    
};