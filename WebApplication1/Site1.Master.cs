﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class Site1 : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string domain = System.Web.HttpContext.Current.User.Identity.Name.Replace(@"\", "/");
            if (domain == "")
            {
                domain = System.Security.Principal.WindowsIdentity.GetCurrent().Name.Replace(@"\", "/");
            }

            string[] sUser = domain.Split('/');
            string sUserId = sUser[1].ToLower();
            Session["user"] = sUserId;
            usr.InnerText = Session["user"].ToString();

        }
    }
}