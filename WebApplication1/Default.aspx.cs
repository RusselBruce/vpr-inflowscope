﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Services;
using System.Web.UI.WebControls;
using System.Data;

namespace WebApplication1
{
    public partial class Default : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        public static string GetData(string user)
        {
            clsConnection cls = new clsConnection();
            string qry = "select top 1 [Property ID] [propID], [Property Address] [propAddress], ";
            qry += "[Municipality Name] [municipality], [Occupancy Status] [occStat], [FC/FCH Codes] [fcCodes], [VPR Service Type] [servType], id [uniqueId], ";
            qry += "Product [prod], POD [pod], [PFC Registration Condition] [pfcRegCond],[REO Registration Condition] [reoRegCond] from dbo.view_VPR_Inflow ";
            qry += "where (ISNULL(TaggingLockTo, '') = '') AND (ISNULL([Registration Process],'') = '') AND Removed is null AND (Status = 'new') OR TaggingLockTo='" + user + "' ORDER BY case when [Prio Tag] is null then 1 else 0 end, [Prio Tag], [Total Aging] desc";

            DataTable dt = cls.GetData(qry);

            if (dt.Rows.Count > 0)
            {
                qry = "select *  from tbl_VPR_Municipality where municipality_name = '" + dt.Rows[0]["municipality"].ToString() + "' and isActive = 1";
                DataTable dtMuni = cls.GetData(qry);
                qry = "update tbl_VPR_CreatedFile set TaggingLockTo='" + user + "'where [Property ID] ='" + dt.Rows[0]["propID"].ToString() + "' and id = '"+dt.Rows[0]["uniqueId"].ToString()+"'";
                cls.ExecuteQuery(qry);
                return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { record = dt, record2 = dtMuni } });
            }

            else{
                return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { record = dt } });
            }
        }
        [WebMethod]
        public static string Save(string munitext, string municode, string regstat, string regprog, 
                                    string reason, string id, string user, string uid, string service)
        {
            clsConnection cls = new clsConnection();

            string statTag = "";
            string serial = "";
            try
            {




                if (service == "Property Registration")
                {
                    service = "PR";
                }
                else if (service == "Renewal")
                {
                    service = "R";
                }
                else if (service.IndexOf("Renewal-") > -1)
                {
                    service = "R" + service.Substring(service.Length - 1, 1);
                }

                string qry = "SELECT [Property ID], REPLACE(CONVERT(varchar, CAST([Created Date] AS datetime), 101),'/','') AS [Created Date] from tbl_VPR_CreatedFile where [Property ID] = '" + id + "' and id = '" + uid + "' and TaggingLockTo = '" + user + "'";

                DataTable dt = cls.GetData(qry);

                string date = dt.Rows[0]["Created Date"].ToString();

                serial = id + "_" + date + "_" + service;

                if (regstat == "On Hold")
                {
                    statTag = "Impediment";
                }
                else if (regstat == "To be registered" && regprog == "Online")
                {
                    statTag = "Online";
                }
                else if (regstat != "To be registered")
                {
                    statTag = "NW";
                }
                else
                {
                    statTag = "";
                }
                string query = "update dbo.tbl_VPR_CreatedFile set [Municipality Name]='" + munitext + "', [Municipality Code]='"
                        + municode + "',[Registration Status]='" + regstat + "',[Registration Process]='" + regprog + "',[Reason]='"
                        + reason + "',[Tagged By]= '" + user + "',[Date Tagged]='" + DateTime.Now.ToString() +
                        "',Status='" +statTag+ "', [Serial Number] = '"+ serial +"' where [Property ID]='" + id + "' and id = '" + uid + "'";

                cls.ExecuteQuery(query);

                string qr = "update tbl_VPR_CreatedFile set TaggingLockTo='' where [Property ID] ='" + id + "' and id = '" + uid + "'";
                cls.ExecuteQuery(qr);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return "True";
        }

        [WebMethod]
        public static string GetMunicipality()
        {
            clsConnection cls = new clsConnection();

            string qry = "select DISTINCT(LTRIM(RTRIM(municipality_name))) [Municipality], municipality_code from tbl_VPR_Municipality where municipality_name != 'CT - Statewide'  and isActive = 1 order by [Municipality]";
            DataTable dt = cls.GetData(qry);

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { record = dt } });
        }

        [WebMethod]
        public static string CloseUpdate(string user)
        {
            clsConnection cls = new clsConnection();

            string query = "update tbl_VPR_CreatedFile set TaggingLockTo='' where TaggingLockTo='"+user+"'";
            cls.ExecuteQuery(query);

            return "Success";
        }

        
    }
}